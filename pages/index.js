import Image from 'next/image';
import styles from '../styles/style.css';

export default function Home() {
  return (
    <div className={style.view}>
      <head>
        <title>indiastockhodl</title>
    <meta name="description" content="
पूरी तरह से सुरक्षित और सुरक्षित निवेश वातावरण के साथ , हमारे साथ निवेश करके पैसा कमाने का अवसर प्रदान करता है। 
हम 2019 से कारोबार में हैं, Investment, Bitcoin Trading, Ethereum Trading, Metaquotes, Earn Money Online,CryptoCurrency Mining" />
    <meta name="viewport" content="width=1200" />
    <meta name="author" content="owner" href="https://indiastockhodl.in" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700" type="text/css" />
      </head>


<div className={styles.navbar}>navbar-expand-lg navbar-light navbar-fixed-top">
 <div className={styles.container}>
<div className={styles.row.center.between.nav.width}>
<div className={styles.mudule_logo}>
<div className={styles.module__logo__link.center.row}><a href='/home'>
<div src={styles/assets/images/svg/logo.svg}>
</div>
</a>
</div>

  
<div className="row pc_no_show header_btn_mobile">
<a href="?a=signup" className="header__link btn">Sign up</a>
<a href="?a=login" className="header__link btn btn_purple">Login</a>
</div>
 
<button className="navbar-toggler b_white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span className="white">
<i className="fas fa-bars"></i>
</span>
</button>
<div className="collapse navbar-collapse menu-navbar" id="navbarSupportedContent">
<nav className="main-menu">
<ul className="navbar-nav ml-auto mt-3 mb-3">
    <li className="nav-item">
<a className="nav-link" href="?a=home">Home</a>
</li>
<li className="nav-item">
<a className="nav-link" href="?a=about">About Us</a>
</li>
<li className="nav-item">
<a className="nav-link" href="?a=offers">Offers</a>
</li>
<li className="nav-item">
<a className="nav-link" href="?a=partnership">Partnership</a>
</li>
<li className="nav-item">
<a className="nav-link" href="?a=faq">Faq</a>
</li>
<li className="nav-item">
<a className="nav-link" href="https://app.indiastockhodl.in/">Exchange</a>
</li>
<li className="nav-item">
<a className="nav-link" href="?a=support">Support</a>
</li>

</ul>
</nav>
  
<div className="mobile_no_show">
<a href="?a=login" className="header__link btn">Login</a>
<a href="?a=signup" className="header__link btn">Sign up</a>
</div>
 
</div>
</div>
</div>
</div>


        <div className={styles.grid}>
          <a href="https://nextjs.org/docs" className={styles.card}>
            <h2>Documentation &rarr;</h2>
            <p>Find in-depth information about Next.js features and API.</p>
          </a>

          <a href="https://nextjs.org/learn" className={styles.card}>
            <h2>Learn &rarr;</h2>
            <p>Learn about Next.js in an interactive course with quizzes!</p>
          </a>

          <a
            href="https://github.com/vercel/next.js/tree/master/examples"
            className={styles.card}
          >
            <h2>Examples &rarr;</h2>
            <p>Discover and deploy boilerplate example Next.js projects.</p>
          </a>

          <a
            href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            className={styles.card}
          >
            <h2>Deploy &rarr;</h2>
            <p>
              Instantly deploy your Next.js site to a public URL with Vercel.
            </p>
          </a>
        </div>


      <footer className={styles.footer}>
        <footer class="footer bg-light" id="bg-light">
<div class="container">
<div class="row">
<div class="col-6 col-md-3">
<h3 class=" title text-white text-left " style="
    font-size: 16px;
        color: #fcb1ab !important;

">COMPANY</h3>
<a class="text-left footnav" href="?a=home">Home</a>
<a class="text-left footnav" href="/?a=about">About</a>
<a class="text-left footnav" href="/?a=rules">Terms</a></div>
<div class="col-6 col-md-3">
<h3 class=" title text-white text-left " style="
    font-size: 16px;
        color: #fcb1ab !important;

">INFO</h3>
<a class="text-left footnav" href="?a=news">News</a>
<a class="text-left footnav" href="/?a=partnership">Partnership</a>
<a class="text-left footnav" href="/?a=offers">Offers</a></div>
<div class="col-6 col-md-3">
<h3 class=" title text-white text-left " style="
    font-size: 16px;
        color: #fcb1ab !important;

">HELP CENTER</h3>
<a class="text-left footnav" href="/?a=faq">FAQ</a>
<a class="text-left footnav" href="/?a=privacy">Privacy</a>
<a class="text-left footnav" href="/?a=support">Support</a>
</div>
<div class="col-6 col-md-3">
<h3 class=" title text-white text-left " style="
    font-size: 16px;
        color: #fcb1ab !important;

">TO STAY CONNECTED</h3>
<li class="d-flex align-items-center">				
						<a href="https://www.facebook.com" target="_blank" class="social-btn d-flex all-center mr-1 mt-1 mr-lg-2 mt-lg-2"><i class="fab fa-facebook-f"></i></a>
						
						<a href="https://twitter.com" target="_blank" class="social-btn d-flex all-center mr-1 mt-1 mr-lg-2 mt-lg-2"><i class="fab fa-twitter"></i></a>
						
						<a href="https://t.me" target="_blank" class="social-btn d-flex all-center mr-1 mt-1 mr-lg-2 mt-lg-2"><i class="fab fa-telegram-plane"></i></a>	
						
            						<a href="https://www.instagram.com/?hl=en"target="_blank" className={social-btn.d-flex.all-center.mr-1.mt-1.mr-lg-2 mt-lg-2} /><i className={fab.fa-instagram} /></i></a>		

					</li>
</div></div></div>
<div className={container.mt-4} />
<div className={row} border-top={1} solid={#eeeeee21} padding-top={22} />
<div className="{col-md-7 my-auto} />
<p>indiastockhodl  © 2021. All Rights Reserved.</p></div>
<div className={col-md-5.my-auto} display={flex}
/>
    <div className={col.my-auto} />

<Image src="bitcoin_PNG13.png"  width={100} />
</div>
												<div className={col.my-auto} />
<Image src="styles/assets/images/png/eth.png" width={100} />
</div>
<div className={col.my-auto} />

<Image src="styles/assets/images/png/141-1410100_free-ripple-cryptocurrency-faucets-litecoin.png" width={100} />
</div>
<div class="col my-auto ">

<Image src="styles/assets/images/png/pmusd.png"  width={100} />
</div>
<div className="col my-auto ">

<Image src="styles/assets/images/png/pyusd.png" width={100} />
</div>

												
</div>
</div>
</div>
</footer>
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    
    </div>
</div>)
}
